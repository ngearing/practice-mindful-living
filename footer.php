<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	</main><!-- #content -->

	<footer id="colophon" class="site-footer">

		<div class="site-info">
			<p>
			<?php printf( esc_html__( '&copy; %1$s %2$s. All rights reserved.', 'ggstyle' ), date( 'Y' ), "<a href='" . esc_url( home_url( '/' ) ) . "'>" . get_bloginfo( 'name' ) . '</a>' ); ?>

			<?php
			if ( function_exists( 'the_privacy_policy_link' ) ) {
				the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
			}
			?>
			</p>

		</div><!-- .site-info -->

		<div class="site-credits">
			<p>
				<a class="designer" href="https://greengraphics.com.au/" rel="designer" target="_blank">Web design.</a>
			</p>
		</div>

		<?php if ( has_nav_menu( 'footer' ) ) : ?>
			<nav class="footer-navigation" aria-label="<?php esc_attr_e( 'Footer Menu', '_s' ); ?>">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'footer',
						'menu_class'     => 'footer-menu',
						'depth'          => 1,
					)
				);
				?>
			</nav><!-- .footer-navigation -->
		<?php endif; ?>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
