<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_s' ); ?></a>
	<div id="page" class="site">

		<header id="masthead" class="site-header">

			<div class="site-branding">
				<?php
				$logo = get_custom_logo();
				if ( ! $logo && file_exists( get_theme_file_path( 'src/images/logo.svg' ) ) ) {
					$logo = '<img alt="' . get_bloginfo() . '" src="' . get_theme_file_uri( 'src/images/logo.svg' ) . '"/>';
				} else {
					$logo = get_bloginfo();
				}

				printf(
					'<%s class="site-title">
						<a href="%s" rel="home">%s</a>
					</%1$s>',
					is_front_page() || is_home() ? 'h1' : 'p',
					esc_url( home_url( '/' ) ),
					$logo
				);
				?>

				<button class="menu-toggle" aria-label="Menu" aria-controls="site-navigation" aria-expanded="false">
					<span class="sr-only">Menu</span>
					<span class="menu-toggle-box">
						<span class="menu-toggle-inner"></span>
					</span>
				</button>

			</div>

			<nav id="site-navigation" class="primary-navigation">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'primary',
						'menu_id'        => 'primary-menu',
						'container'      => 'ul',
					)
				);
				?>
			</nav><!-- #site-navigation -->

	</header>

	<main id="content" class="site-content">
