<?php

$actions = get_field( 'calls_to_action' );
if ( ! $actions ) {
	return;
}
?>
<section class="calls-to-action">

	<?php foreach ( $actions as $action ) : ?>
		<div class="action">
			<a href="<?php echo $action['link']; ?>">
				<h2><?php echo $action['title']; ?></h2>
			</a>
		</div>
	<?php endforeach; ?>

</section>
