<?php
$workshops = get_field( 'workshops' );
if ( ! $workshops ) {
	return;
}
?>

<div class="slider workshop-slider">

	<?php
	foreach ( $workshops as $workshop ) :
		?>

	<div class="slide">
		<?php
		if ( isset( $workshop['image'] ) ) {
			printf(
				'<div class="slide-image">%s</div>',
				wp_get_attachment_image( $workshop['image'], 'medium' )
			);
		}
		?>

		<h3><?php echo $workshop['title']; ?></h3>
		<h4><?php echo $workshop['date']; ?></h4>

		<div class="slide-text">
			<?php echo apply_filters( 'the_content', $workshop['quote'] ); ?>
			<?php echo apply_filters( 'the_content', $workshop['text'] ); ?>
		</div>

		<?php
		if ( isset( $workshop['buttons'] ) ) {
			foreach ( $workshop['buttons'] as $button ) :
				?>
			<a class="button" href="<?php echo $button['link']; ?>"><?php echo $button['text']; ?></a>
				<?php
			endforeach;
		}
		?>
	</div>

	<?php endforeach; ?>
</div>
