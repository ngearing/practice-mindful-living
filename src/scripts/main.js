import "../styles/main.scss"
import "./modules/menu-toggle"
import { noOver } from "./modules/noOver"

;(function() {
  "use strict"

  var header = document.querySelector(".site-header")
  var menu = document.querySelector(".primary-navigation")
  noOver(menu, header)
})()
