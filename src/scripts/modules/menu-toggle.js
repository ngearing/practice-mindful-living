;(function() {
  var toggles = document.querySelectorAll(".menu-toggle")
  if (!toggles) {
    return
  }

  document.addEventListener("click", function(event) {
    Array.from(toggles).forEach(tog => {
      if (!event.path && !event.composedPath())
        console.log("Browser does not support path or composedPath()")
      var inPath =
        event.path && event.path.indexOf(tog) > -1
          ? true
          : false ||
            (event.composedPath() && event.composedPath().indexOf(tog) > -1)
          ? true
          : false
      var menu =
        tog.getAttribute("aria-controls") || tog.getAttribute("data-target")
      menu = document.querySelector(`#${menu}`)
      if (!menu) {
        console.log(`Could not find menu #${menu}`)
        return
      }

      if (inPath) {
        toggleMenu(tog, menu)
      } else {
        var inMenu = event.path.indexOf(menu) > -1 ? true : false
        if (!inMenu && tog.classList.contains("toggled")) {
          toggleMenu(tog, menu)
        }
      }
    })
  })

  function toggleMenu(ele, menu) {
    console.log(ele)

    ele.classList.toggle("toggled")
    menu.classList.toggle("toggled")

    ele.setAttribute("aria-expanded", !!ele.getAttribute("aria-expanded"))
  }
})()
