export var noOver = function(ele, avoid) {
  var elePos = ele.getBoundingClientRect()
  var avoidPos = avoid.getBoundingClientRect()

  var avoidIsFixed = avoid.style.position == "fixed" ? true : false

  avoid.addEventListener("transitionend", hasMutated)

  if (!avoidIsFixed) {
    window.addEventListener("scroll", hasMutated)
  }

  var mutObs = new MutationObserver(hasMutated)
  mutObs.observe(avoid, {
    attributes: true,
    childList: true,
    subtree: true,
  })

  function hasMutated(list, obs) {
    window.requestAnimationFrame(checkPos)
  }

  function checkPos() {
    var newPos = avoid.getBoundingClientRect()
    var diff = []

    // Have to use for ... in here as getBounding cant use filter.
    for (const key in newPos) {
      if (avoidPos[key] !== newPos[key]) {
        diff.push(key)
      }
    }

    if (diff) {
      avoidPos = newPos
      updatePos(avoid)
    }
  }

  function updatePos(avoid) {
    var top = avoidPos.y + avoidPos.height

    if (top > 0) {
      ele.style.top = top + "px"
    } else if (ele.style.top != 0) {
      ele.style.top = "0px"
    }
  }
}
