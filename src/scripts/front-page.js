import "../styles/4-pages/front-page.scss"
;(function($) {
  $(".slider").slick({
    variableWidth: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    rows: false,
  })
})(jQuery)
