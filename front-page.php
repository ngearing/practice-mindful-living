<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

get_header();
?>

<?php get_template_part( 'templates/partials/workshop-slider' ); ?>

<?php
while ( have_posts() ) :
	the_post();

	get_template_part( 'templates/content', 'page' );

endwhile; // End of the loop.
?>

<?php get_template_part( 'templates/partials/action-buttons' ); ?>

<?php
get_footer();
